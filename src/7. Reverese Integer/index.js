/**
 * @param {number} x
 * @return {number}
 */
const reverse = function(x) {
    const str = String(x);
    let reversed = '';
    let hasMinus = false;
    for(let i = str.length - 1; i >= 0; i--){
        const value = str[i];

        if(i === str.length && value === '0')
            continue;

        if(i === 0 && value === '-')
            hasMinus = true;

        else
            reversed += value;
    }

    return Number(hasMinus ? `-${reversed}` : reversed);
};

console.log(reverse(123))
console.log(reverse(-123))
console.log(reverse(120))